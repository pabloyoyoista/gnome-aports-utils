#!/bin/sh
#
# Copyright (C) 2022 Pablo Correa Gomez
#

source ./packages

for package in $ALL; do
    grep -q "pkgname-lang" $package/APKBUILD || echo $package
done
