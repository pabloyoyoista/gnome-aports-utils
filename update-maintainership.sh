#!/bin/sh
#
# Copyright (C) 2023 Pablo Correa Gomez
#

set -ex

source ./packages

die () {
    echo $1
    return 1
}

GIT="git --git-dir=${APORTSDIR}/.git --work-tree=${APORTSDIR}"
APORTSDIR=${APORTSDIR:-"$HOME/aports"}
test -d $APORTSDIR || die "A directory is needed, $APORTSDIR does not exist "

for app in $GNOME_CORE_APPS $GNOME_SYSTEM; do
    APKBUILD="$APORTSDIR/community/$app/APKBUILD"
    sed -i -e 's%Maintainer: Rasmus Thomsen <oss@cogitri.dev>%Maintainer: team/gnome <ablocorrea@hotmail.com>%1' $APKBUILD
    apkgrel --add $APKBUILD
    $GIT add $APKBUILD
    $GIT commit --message "community/$app: update maintainer to team/gnome"
done
